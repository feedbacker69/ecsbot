import { DateTime, Interval } from "luxon";
import { FilteredStreams, Stream } from "./types";

// To have consistent filtering, all filter functions take "now"
// which is a DateTime

function _filterStreams(schedule: Stream[], lower: DateTime, upper: DateTime) {
  const interval = Interval.fromDateTimes(lower, upper);
  const filteredStreams = schedule.filter((stream) =>
    interval.contains(stream.dateTime)
  );

  return filteredStreams;
}

function filterSoonPlayingStreams(schedule: Stream[], now: DateTime) {
  const lower = now.minus({ hours: 1, minutes: 30 }); // a set is slated to go usually for like 1.5 hr
  const upper = now.plus({ hours: 8 }); // Filter next 8 hours

  return _filterStreams(schedule, lower, upper);
}

function filterStreamsPlayingNow(schedule: Stream[], now: DateTime) {
  const lower = now.minus({ hours: 1, minutes: 30 });
  const upper = now;

  return _filterStreams(schedule, lower, upper);
}

function filterStreamsPlayingInAnHour(schedule: Stream[], now: DateTime) {
  const lower = now;
  const upper = now.plus({ hours: 1 });

  return _filterStreams(schedule, lower, upper);
}

function filterStreamsPlayingLaterThanAnHour(
  schedule: Stream[],
  now: DateTime
) {
  const lower = now.plus({ hours: 1 });
  const upper = now.plus({ hours: 8 });

  return _filterStreams(schedule, lower, upper);
}

export function filterSchedule(schedule: Stream[]): FilteredStreams {
  const now = DateTime.now();
  const filteredStreams = filterSoonPlayingStreams(schedule, now);

  const nowPlayingStreams = filterStreamsPlayingNow(filteredStreams, now);
  const startsInOneHourStreams = filterStreamsPlayingInAnHour(
    filteredStreams,
    now
  );

  // Limit otherPlayers to max 15 players
  let otherStreams = filterStreamsPlayingLaterThanAnHour(filteredStreams, now);
  if (otherStreams.length > 15) otherStreams = otherStreams.slice(0, 15);

  return { nowPlayingStreams, startsInOneHourStreams, otherStreams };
}
