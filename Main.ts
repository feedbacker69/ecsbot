import { downloadSchedule } from "./ECSClient";
import { parseSchedule } from "./Parser";
import { filterSchedule } from "./Schedule";
import { renderPost } from "./DiscordPostRenderer";
import { DiscordClient } from "./DiscordClient";
import isEqual from "lodash/isEqual";
import { token, serverName, channelName, scheduleUrl } from "./env.json";
import { FilteredStreams } from "./types";
import { nothingToPost } from "./util";

let lastFilteredStreams: FilteredStreams;

async function task() {
  const src = await downloadSchedule(scheduleUrl);
  console.log("schedule downloaded");
  const schedule = parseSchedule(src);
  console.log("schedule parsed");
  const filteredStreams = filterSchedule(schedule);
  console.log("filtered");

  if (isEqual(lastFilteredStreams, filteredStreams)) {
    console.log("no diff. not posting.");
    return;
  }

  lastFilteredStreams = filteredStreams;

  if (nothingToPost(filteredStreams)) {
    console.log("nobody playing. not posting.");
    return;
  }

  const { message, embed } = renderPost(scheduleUrl, filteredStreams);
  console.log("rendering");
  await discordClient.post(message, embed);
  console.log("posted");
}

const discordClient = new DiscordClient(token, serverName, channelName, task);
