import { DateTime } from "luxon";
import { FilteredStreams, Stream } from "./types";

export function twitchLinkTextGenerator(streams: Stream[]) {
  return streams
    .map(
      (player) =>
        `**[${player.name}](https://www.twitch.tv/${
          player.twitchUserName
        })** @ <t:${player.dateTime.toSeconds()}:t>`
    )
    .join("\n");
}

export function parseEasternTime(SQLDateTime: string) {
  return DateTime.fromSQL(SQLDateTime).setZone("America/New_York", {
    keepLocalTime: true,
  });
}

export function nothingToPost(filteredStreams: FilteredStreams) {
  const { nowPlayingStreams, startsInOneHourStreams, otherStreams } =
    filteredStreams;

  return (
    nowPlayingStreams.length === 0 &&
    startsInOneHourStreams.length === 0 &&
    otherStreams.length === 0
  );
}
