import { Stream } from "./types";
import { parseEasternTime } from "./util";

export function parseSchedule(src: string) {
  const lines = src.split("\n");
  const pattern = "\\s+scheduleTwitchBlock\\('(.+)', '(.+)', '(.+)'\\)";

  const newSchedule: Stream[] = [];

  for (const line of lines) {
    const result = line.match(pattern);

    if (result) {
      let [_, name, dateTime, twitchUserName] = result;
      twitchUserName = twitchUserName.replace("https://www.twitch.tv/", ""); //antishoutouts to Urza

      newSchedule.push({
        name,
        dateTime: parseEasternTime(dateTime),
        twitchUserName,
      });
    }
  }

  console.log("parsed");
  return newSchedule;
}
