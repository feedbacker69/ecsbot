import { DateTime } from "luxon";

export interface Stream {
  name: string;
  dateTime: DateTime;
  twitchUserName: string;
}

export interface FilteredStreams {
  nowPlayingStreams: Stream[];
  startsInOneHourStreams: Stream[];
  otherStreams: Stream[];
}
