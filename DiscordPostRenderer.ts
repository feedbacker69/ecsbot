import { EmbedBuilder } from "discord.js";
import { twitchLinkTextGenerator } from "./util";
import { FilteredStreams } from "./types";

export function renderPost(
  scheduleUrl: string,
  filteredStreams: FilteredStreams
) {
  // Set the default message embed
  const embed = new EmbedBuilder({
    title: "ECS11 Set Schedule Announcement",
    url: scheduleUrl,
    description:
      "Click on the name (or right click and copy link) to go to their Twitch profiles. Times are in your local time.",
  });
  let message;

  const { nowPlayingStreams, startsInOneHourStreams, otherStreams } =
    filteredStreams;

  if (nowPlayingStreams.length > 0) {
    const nowPlayingText = nowPlayingStreams
      .map(
        (player) =>
          `**${player.name}** (<https://www.twitch.tv/${player.twitchUserName}>)`
      )
      .join(", ");

    const verb = nowPlayingStreams.length >= 2 ? "are" : "is"; // 0 doesn't matter because it's not gonna get rendered
    const s = nowPlayingStreams.length >= 2 ? "s" : "";
    message = `${nowPlayingText} ${verb} scheduled to play right now. Come support their set${s}!`;

    embed.addFields({
      name: "Players playing right now",
      value: twitchLinkTextGenerator(nowPlayingStreams),
    });
  }

  if (startsInOneHourStreams.length > 0) {
    embed.addFields({
      name: "Players playing within an hour",
      value: twitchLinkTextGenerator(startsInOneHourStreams),
    });
  }

  if (otherStreams.length > 0) {
    embed.addFields({
      name: "Players playing soon",
      value: twitchLinkTextGenerator(otherStreams),
    });
  }

  return { message, embed };
}
