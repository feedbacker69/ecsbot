import axios from "axios";
export async function downloadSchedule(url) {
  const response = await axios.get(url, { responseType: "text" });
  return response.data;
}
