import {
  ActivityType,
  ChannelType,
  Client,
  Embed,
  EmbedBuilder,
  GatewayIntentBits,
  Guild,
  GuildTextBasedChannel,
} from "discord.js";

export class DiscordClient {
  token: string;
  client: Client;
  serverName: string;
  channelName: string;
  server: Guild;
  channel: GuildTextBasedChannel;
  task: Function;

  constructor(
    token: string,
    serverName: string,
    channelName: string,
    task: Function
  ) {
    this.client = new Client({ intents: [GatewayIntentBits.Guilds] });

    this.token = token;
    this.serverName = serverName;
    this.channelName = channelName;
    this.task = task;

    this.client.login(this.token);
    console.log("logging in");
    this.client.on("ready", this.onReady.bind(this));
  }

  async setUserActivity() {
    if (this.client.user) {
      await this.client.user.setActivity("East Coast Stamina 11", {
        type: ActivityType.Watching,
      });
      console.log(`activity set`);
    }
  }

  async findServer(serverName: string) {
    const serverID = this.client.guilds.cache.findKey(
      (guild) => guild.name === serverName
    );

    if (!serverID) throw new Error("Server Not Found");
    this.server = await this.client.guilds.fetch(serverID);
    console.log("Server found");
  }

  async findChannel(channelName: string) {
    const channel = await this.server.channels.cache.find(
      (c) => c.name === channelName
    );

    if (!channel) throw new Error("Channel not found!");
    if (channel.type !== ChannelType.GuildText)
      throw new Error("Wrong channel type!");

    this.channel = channel as GuildTextBasedChannel;
    console.log("Channel found");
  }

  async onReady() {
    await this.setUserActivity();
    await this.findServer(this.serverName);
    await this.findChannel(this.channelName);
    await this.task();
    setInterval(this.task, 15 * 60 * 1000);
    console.log("the loop begins");
  }

  async post(message: string, embed: EmbedBuilder) {
    await this.channel.send({ content: message, embeds: [embed] });
  }
}

module.exports = { DiscordClient };
